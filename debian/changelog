python-vagrant (1.1.0-2) unstable; urgency=medium

  * Team upload.
  * d/control: build-dep on setuptools-scm to fix wrong "0.0.0" version
  * Standards-Version: 4.6.2 (routine-update)
  * Reorder sequence of d/control fields by cme (routine-update)
  * Build-Depends: s/dh-python/dh-sequence-python3/ (routine-update)
  * Rules-Requires-Root: no (routine-update)
  Set upstream metadata fields: Bug-Database, Bug-Submit, Repository, Repository-Browse.
  * watch file standard 4 (routine-update)

 -- Michael R. Crusoe <crusoe@debian.org>  Fri, 26 Apr 2024 18:03:20 +0200

python-vagrant (1.1.0-1) unstable; urgency=medium

  * New upstream version 1.1.0

 -- Hans-Christoph Steiner <hans@eds.org>  Mon, 06 Feb 2023 01:08:42 +0100

python-vagrant (1.0.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.5.1, no changes needed.

  [ Hans-Christoph Steiner ]
  * New upstream version 1.0.0
  * upstream follows PEP 517 using pyproject.toml

 -- Hans-Christoph Steiner <hans@eds.org>  Thu, 13 Oct 2022 23:16:40 +0200

python-vagrant (0.5.15-4) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.5.1, no changes needed.

 -- Sandro Tosi <morph@debian.org>  Sat, 04 Jun 2022 16:02:05 -0400

python-vagrant (0.5.15-3) unstable; urgency=medium

  * Team upload.
  * Use debhelper-compat instead of debian/compat.
  * Drop Python 2 support.
  * Use pybuild for building package.
  * Enable autopkgtest-pkg-python testsuite.
  * Bump debhelper compat level to 12.
  * Bump standards version to 4.4.0 (no changes).
  * Add vagrant to runtime depends.

 -- Ondřej Nový <onovy@debian.org>  Sun, 11 Aug 2019 15:14:07 +0200

python-vagrant (0.5.15-2) unstable; urgency=medium

  [ Chris Lamb ]
  * Team upload.
  * Remove trailing whitespaces.
  * Bump Standards-Version to 4.2.1.
  * Bump debhelper compat level to 11.
  * wrap-and-sort -sa.

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * Convert git repository from git-dpm to gbp layout

 -- Chris Lamb <lamby@debian.org>  Tue, 18 Sep 2018 18:31:18 +0100

python-vagrant (0.5.15-1) unstable; urgency=medium

  * New upstream version

 -- Hans-Christoph Steiner <hans@eds.org>  Mon, 08 Jan 2018 22:07:40 +0100

python-vagrant (0.5.14-1) unstable; urgency=medium

  * latest upstream release

 -- Hans-Christoph Steiner <hans@eds.org>  Fri, 17 Jun 2016 13:57:33 +0200

python-vagrant (0.5.13-1) unstable; urgency=low

  * Initial release. (Closes: #827514)

 -- Hans-Christoph Steiner <hans@eds.org>  Thu, 16 Jun 2016 21:52:06 +0200
